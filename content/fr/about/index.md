---
title: "A propos"
description: "L’AAPPMA La Pelaude s’étend sur le haut bassin de la rivière Vienne, à l’est du département de la Haute-vienne, aux limites de la Creuse et de la Corrèze"
featured_image: '/images/vienne_maspecou.jpg'
menu:
  main:
    weight: 1
---
{{< figure src="../images/logopelaude1.jpg" title="L’AAPPMA La Pelaude" >}}

L’AAPPMA La Pelaude s’étend sur le haut bassin de la rivière Vienne, à l’est du 
département de la Haute-vienne, aux limites de la Creuse et de la Corrèze. Son territoire inclut, de l’amont vers l’aval,  les  communes  de  Rempnat,  Nedde,  Beaumont-du-Lac,  Eymoutiers,  Saint-Amand-le Petit et Augne.
Préservé des barrages et de l’urbanisation, ce bassin, haut lieu historique de la pêche à la truite, offre aux pêcheurs prés de 40 km de rivière et des ruisseaux de diverses tailles et une eau  de  qualité.  Les  amateurs  de  pêche  en  eau  dormante (poissons  blancs  et  carnassiers) pourront également profiter de la proximité des grands barrages EDF de la Maulde, en particulier le lac de Vassiviére. Notre territoire offre ainsi aux pêcheurs une grande variété de milieux.
Il existe aussi juste en aval d’Eymoutiers une portion de rivière vouée à la pêche sans tuer, où toutes les truites et ombres capturés doivent être relâchés, quelle que soit leur taille.
Depuis vingt ans déjà, notre AAPPMA a choisi la gestion patrimoniale. Aucune truite 
née hors de notre rivière n’y a été déversée. Ce choix nous permet d’avoir sur tout le bassin une population de véritables truites sauvages, un poisson hélas devenu trop rare, et que nous sommes fiers d’avoir pu protéger de sa disparition presque généralisée.
Améliorer les conditions de vie et favoriser le frai de ce précieux patrimoine est donc 
notre  principal  objectif.  L’essentiel  se  fait  naturellement,  mais lentement.  Nous  avons cependant  le  plaisir de  recenser,  tous  les  ans,  lors  de  pêches électriques organisées  avec  la Fédération Départementale, des truites sauvages de prés de 50 cm !
Les programmes d’entretien et de restauration  jouent pour beaucoup dans  la capacité d’accueil du milieu. Ces grands travaux, gérés principalement par le Syndicat Intercommunal Monts et Barrages, sont d’un grand secours pour notre AAPPMA.
Une  AAPPMA de  notre  importance  a  aujourd’hui  de  grands besoins en  termes de gestion piscicole. Il y a toujours beaucoup à faire, et l’apport de moyens humains et financiers est évidemment bienvenu. C’est ainsi qu’en nous rejoignant et en adhérant à notre AAPPMA, vous ne serez pas qu’un usager du milieu piscicole, mais un militant, en contribuant à soutenir la gestion d’un territoire de qualité et la préservation d’un patrimoine collectif.
