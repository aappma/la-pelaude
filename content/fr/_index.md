---
title: "AAPPMA La Pelaude"

description: "Site exemple de La Pelaude"
cascade:
  featured_image: '/images/vienne_maspecou.jpg'
---

Exemple de blabla d accueil.

Ce site est propulsé par [GitLab Pages](https://about.gitlab.com/features/pages/)
et [Hugo](https://gohugo.io), il utilise le theme [`ananke` theme](https://github.com/theNewDynamic/gohugo-theme-ananke).

